<!-- PROJECT SHIELDS -->

[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![pipeline status][pipeline-shield]][pipeline-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/sophiabrandt/django-rest-tdd">
    <img src="logo.png">
  </a>

  <h3 align="center">DJANGO REST TDD</h3>

  <p align="center">
    a RESTful movie API, using Test-Driven Development
    <br />
    <a href="https://github.com/sophiabrandt/django-rest-tdd"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/sophiabrandt/django-rest-tdd">View Demo</a>
    ·
    <a href="https://github.com/sophiabrandt/django-rest-tdd/issues">Bugs</a>
    ·
    <a href="https://github.com/sophiabrandt/django-rest-tdd/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [About the Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [Live Demo](#live-demo)
- [Roadmap](#roadmap)
- [Contributing](#contributing)
- [License](#license)
- [Contact](#contact)
- [Acknowledgements](#acknowledgements)

<!-- ABOUT THE PROJECT -->

## About The Project

Code for [Test-Driven Development with Django, Django REST Framework, and Docker][tdddjango]. Learning project, (almost) no original code.

### Built With

- Django
- Django REST Framework
- Docker

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these steps:

### Prerequisites

- Python 3.7
- Docker 19.03
- docker-compose 1.25

### Installation

1. Clone the django-rest-tdd

   ```sh
   git clone https://github.com/sophiabrandt/django-rest-tdd.git
   ```

   Or - if you have node installed - use degit:

   ```sh
   npx degit https://github.com/sophiabrandt/django-rest-tdd.git django-rest-tdd
   ```

2. Build the containers:

   Create a new file `app/.env.dev` with the following content:

   ```
   DEBUG=1
   SECRET_KEY=foo
   DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 [::1]
   ```

   Then run:

   ```sh
   docker-compose build
   ```

   Start the application:

   ```sh
   docker-compose up -d
   ```

3. Apply database migrations:

   ```sh
   docker-compose exec movies python manage.py migrate --noinput
   ```

4. Create the initial data:

   ```sh
   docker-compose exec movies python manage.py loaddata movies.json
   ```

   (You can reset the database with `docker-compose exec movies python manage.py flush`).

5. Create a superuser for the admin dashboard:
   ```sh
   docker-compose exec movies python manage.py createsuperuser
   ```

<!-- USAGE EXAMPLES -->

## Usage

Run the application with `docker-compose up -d`.

Navigate to `http://localhost:8000/admin` for the admin dashboard.

### Tests

Run tests with:

```sh
docker-compose exec movies pytest -p no:cacheprovider
```

### Code Quality

1. Linting
   ```sh
   docker-compose exec movies flake8 .
   ```
2. Code style

   ```sh
   docker-compose exec movies black --check --exclude=migrations .
   ```

3. Sort imports

   Check:

   ```sh
   docker-compose exec movies /bin/sh -c "isort ./*/*.py --check-only"
   ```

   Sort:

   ```sh
   docker-compose exec movies /bin/sh -c "isort ./*/*.py"
   ```

<!-- LIVE DEMO -->

## Live Demo

You can find a live demo of the application on [Heroku](https://safe-plateau-41271.herokuapp.com/ping).

Example Routes:

- [/ping](https://safe-plateau-41271.herokuapp.com/api/movies/)
- [/api/movies](https://safe-plateau-41271.herokuapp.com/api/movies/)
- [/docs](https://safe-plateau-41271.herokuapp.com/docs/)
- [/swagger-docs](https://safe-plateau-41271.herokuapp.com/swagger-docs/)

<!-- ROADMAP -->

## Roadmap

See the [open issues](https://github.com/sophiabrandt/django-rest-tdd/issues) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->

## License

Code is &copy; Michael Herman, with minor modifications by Sophia Brandt.

<!-- CONTACT -->

## Contact

Sophia Brandt - [@hisophiabrandt](https://twitter.com/hisophiabrandt)

Project Link: [https://github.com/sophiabrandt/django-rest-tdd](https://github.com/sophiabrandt/django-rest-tdd)

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [Test-Driven Development with Django, Django REST Framework, and Docker by Michael Herman][tdddjango]
- [Best-README-Template by Othneil Drew](https://github.com/othneildrew/Best-README-Template)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/github/contributors/sophiabrandt/django-rest-tdd.svg?style=flat-square
[contributors-url]: https://github.com/sophiabrandt/django-rest-tdd/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/sophiabrandt/django-rest-tdd.svg?style=flat-square
[forks-url]: https://github.com/sophiabrandt/django-rest-tdd/network/members
[stars-shield]: https://img.shields.io/github/stars/sophiabrandt/django-rest-tdd.svg?style=flat-square
[stars-url]: https://github.com/sophiabrandt/django-rest-tdd/stargazers
[issues-shield]: https://img.shields.io/github/issues/sophiabrandt/django-rest-tdd.svg?style=flat-square
[issues-url]: https://github.com/sophiabrandt/django-rest-tdd/issues
[license-shield]: https://img.shields.io/github/license/sophiabrandt/django-rest-tdd.svg?style=flat-square
[tdddjango]: https://testdriven.io/courses/tdd-django/
[pipeline-shield]: https://gitlab.com/sophiabrandt/django-rest-tdd/badges/master/pipeline.svg?style=flat-square
[pipeline-url]: https://gitlab.com/sophiabrandt/django-rest-tdd/commits/master
